/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.ox_oop;

/**
 *
 * @author focus
 */
public class Board {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer;
    private Player o;
    private Player x;
    private int count;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlayer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() { // count จะเปลี่ยนเมื่อมีการเล่นแล้วเปลี่ยน turn
        return count;
    }

    private void switchPlayer() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if(isWin() || isDraw()){
            return false;
        }
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlayer.getSymbol();
        if (checkWin(row, col)) {
            updateStat();
            this.win = true;
            return true;
        }

        if (checkDraw()) {
            o.draw();
            x.draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }

    private void updateStat() {
        if (this.currentPlayer == o) {
            o.win();
            x.lose();
        } else {
            x.win();
            o.lose();
        }
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }

    public boolean checkWin(int row, int col) {
        if (checkVertical(col)) {
            return true;
        } else if (checkHorizontal(row)) {
            return true;
        } else if (checkCross()) {
            return true;
        }
        return false;
    }

    public boolean checkVertical(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer.getSymbol()) { //ถ้ามีทุกอันที่ไม่เท่ากับผู้เล่นปัจจุบันให้return false
                return false;
            }
        }
        return true; //ถ้าในแถวแนวตั้งมีเท่าผู้เล่นปัจจุบันให้ return true
    }

    public boolean checkHorizontal(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCross() {
        if (checkX1()) {
            return true;
        } else if (checkX2()) {
            return true;
        }
        return false;
    }

    public boolean checkX1() { // 1 1, 2 2, 3 3
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2() {// 1 3, 2 2, 3 1
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer.getSymbol()) {
                return false;
            }
        }
        return true;
    }
}
